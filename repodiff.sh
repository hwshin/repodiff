#!/bin/sh

function get_repo_list {
    cd $1
    NWD=`pwd`
    repolist=`repo list -p`
    for dir in $repolist ; do
        cd $dir
        lastcommit=`git log | grep commit | head -1`
        echo $dir $lastcommit
        cd $NWD
    done
}

function show_repo_difflist {
    diff -ru $1 $2 >& $3
    cat $3 | grep ^[+-][a-z]
}

function usage {
    echo "Usage:$1 <master_branch_path> <release_branch_path>"
    exit
}

#master_path="/home/amf/Work/ENTH_RDK/twc-rdk-root.awing.master.new"
#release_path="/home/amf/Work/ENTH_RDK/twc-rdk-root.awing.twc_rel-1.new"

argc=$#
if ! [ $argc -eq "2" ] ; then
    usage $0
fi
if ! [ -d $1 ]; then
    echo "'$1' does not exist"
    usage $0
fi
if ! [ -d $2 ]; then
    echo "'$2' does not exist"
    usage $0
fi

master_path=$1
release_path=$2

master_list_file="/tmp/master"
release_list_file="/tmp/release"
diff_list_file="/tmp/diff"

get_repo_list $master_path >& $master_list_file
get_repo_list $release_path >& $release_list_file

show_repo_difflist $master_list_file $release_list_file $diff_list_file

rm $master_list_file $release_list_file $diff_list_file
